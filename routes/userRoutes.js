import express from 'express';
const router = express.Router();
import { body, param } from 'express-validator';
import {
  createUser,
  getAllUsers,
  getSingleUser,
  getFriends,
} from '../controllers/userController.js';
import handleError from '../errors/error-handler.js';

router.post(
  '/',
  body('name')
    .trim()
    .isLength({ min: 5 })
    .withMessage('Name must be of atleast 3 characters'),
  body('email').isEmail().withMessage('Email is required'),
  handleError,
  createUser
);
router.get('/', getAllUsers);
router.get('/:id', param('id').isMongoId(), handleError, getSingleUser);
router.get('/:id/friends', param('id').isMongoId(), handleError, getFriends);

export default router;
