import express from 'express';
const router = express.Router();
import {
  createRequest,
  getAllRequests,
  getUserSentRequest,
  getUserReceivedRequest,
  updateRequest,
  getSingleRequest,
  deleteRequest,
} from '../controllers/requestController.js';

import { body, param } from 'express-validator';
import handleError from '../errors/error-handler.js';

router.post(
  '/',
  body('senderId').isMongoId(),
  body('receiverId').isMongoId(),
  handleError,
  createRequest
);
router.get('/all', getAllRequests);
router.post(
  '/getSentRequests',
  body('userId').isMongoId(),
  handleError,
  getUserSentRequest
);
router.post(
  '/getReceivedRequests',
  body('userId').isMongoId(),
  handleError,
  getUserReceivedRequest
);
router.get('/:id', param('id').isMongoId(), handleError, getSingleRequest);
router.patch(
  '/:id',
  param('id').isMongoId(),
  body('receiverId').isMongoId(),
  handleError,
  updateRequest
);
router.delete(
  '/:id',
  param('id').isMongoId(),
  body('senderId').isMongoId(),
  handleError,
  deleteRequest
);

export default router;
