import mongoose from 'mongoose';
import validator from 'validator';

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'Please provide name'],
    minlength: 5,
    maxlength: 20,
  },
  email: {
    type: String,
    validate: {
      validator: validator.isEmail,
      message: 'Please provide a valid email',
    },
    required: [true, 'Please provide email'],
    unique: true,
  },
  friends: {
    type: [mongoose.Types.ObjectId],
    ref: 'User',
  },
});

export default mongoose.model('User', UserSchema);
