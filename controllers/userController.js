import mongoose from 'mongoose';
import User from '../models/User.js';

const createUser = async (req, res) => {
  const { name, email } = req.body;

  const emailExists = await User.findOne({ email });

  if (emailExists) {
    return res.status(400).json({ message: 'User with email already exists' });
  }

  const user = await User.create({ name, email });

  res.status(201).json(user);
};
const getAllUsers = async (req, res) => {
  const users = await User.find({});

  res.status(200).json(users);
};
const getSingleUser = async (req, res) => {
  const { id: userId } = req.params;

  const user = await User.findOne({ _id: userId });

  if (!user) {
    return res.status(400).json({ msg: 'User not found' });
  }

  res.status(200).json(user);
};

const getFriends = async (req, res) => {
  const { id: userId } = req.params;

  const user = await User.findOne({ _id: userId }).populate('friends');
  if (!user) {
    return res.status(400).json({ msg: 'User not found' });
  }

  res.status(200).json(user.friends);
};

export { createUser, getAllUsers, getSingleUser, getFriends };
