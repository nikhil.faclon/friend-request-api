import User from '../models/User.js';
import Request from '../models/Request.js';
import mongoose from 'mongoose';

const createRequest = async (req, res) => {
  const { senderId, receiverId } = req.body;

  if (senderId === receiverId) {
    return res
      .status(400)
      .json({ msg: 'Sender ID and Receiver ID must be different' });
  }

  const senderUser = await User.findOne({ _id: senderId });
  const receiverUser = await User.findOne({ _id: receiverId });

  if (!senderUser || !receiverUser) {
    return res.status(400).json({ msg: 'Sender or Receiver does not exist' });
  }

  let alreadyFriends = false;

  senderUser.friends.forEach((friend) => {
    if (friend._id.toString() == receiverId) {
      alreadyFriends = true;
    }
  });

  if (alreadyFriends) {
    return res.status(400).json({ msg: 'Already friends' });
  }

  const request = await Request.create({
    sender: senderId,
    receiver: receiverId,
  });

  res.status(201).json({ msg: 'works' });
};

const getAllRequests = async (req, res) => {
  const requests = await Request.find({});

  res.status(200).json(requests);
};

const getUserSentRequest = async (req, res) => {
  const { userId } = req.body;

  const requestsSent = await Request.find({ sender: userId });

  console.log(requestsSent);
  res.status(200).json(requestsSent);
};
const getUserReceivedRequest = async (req, res) => {
  const { userId } = req.body;

  if (!userId) {
    return res.status(400).json({ msg: 'Please provide userID' });
  }

  console.log(userId);
  const requestsReceived = await Request.find({ receiver: userId });

  res.status(200).json(requestsReceived);
};

const getSingleRequest = async (req, res) => {
  const { id: requestId } = req.params;
  const requestExists = await Request.findOne({ _id: requestId });

  if (!requestExists) {
    return res.status(400).json({ msg: `Request with ${requestId} not found` });
  }

  res.status(200).json(requestExists);
};

const updateRequest = async (req, res) => {
  const { id: requestId } = req.params;
  const { receiverId, status } = req.body;

  if (status !== 'accepted' && status !== 'rejected') {
    return res
      .status(400)
      .json({ msg: 'Status can only change to accepted or rejected' });
  }

  const requestExists = await Request.findOne({ _id: requestId });

  if (!requestExists) {
    return res.status(400).json({ msg: `Request with ${requestId} not found` });
  }

  const senderId = requestExists.sender;

  const sender = await User.findOne({ _id: senderId });

  const receiver = await User.findOne({
    _id: receiverId,
  });

  if (!sender || !receiver) {
    return res.status(400).json({ msg: 'Sender or Receiver does not exist' });
  }

  // only receiver can update request
  const validUser = requestExists.receiver.toString() === receiverId;

  if (!validUser) {
    return res
      .status(400)
      .json({ msg: 'You not not authorized to access this route' });
  }

  // if accepted  then update friendsList
  const session = await mongoose.startSession();

  try {
    session.startTransaction();
    if (status == 'accepted') {
      await sender.updateOne({ $push: { friends: receiver } }).session(session);

      await receiver.updateOne({ $push: { friends: sender } }).session(session);

      // throw new Error();

      await Request.findOneAndDelete({ _id: requestId }).session(session);
    }

    await session.commitTransaction();
  } catch (error) {
    await session.abortTransaction();
    return res.status(400).json(error);
  } finally {
    session.endSession();
  }

  res.status(200).json({ msg: `Request ${status}` });
};

const deleteRequest = async (req, res) => {
  const { id: requestId } = req.params;
  const requestExists = await Request.findOne({ _id: requestId });
  const { senderId } = req.body;

  const sender = await User.findOne({ _id: senderId });
  if (!sender) {
    return res.status(400).json({ msg: 'Sender does not exist' });
  }

  if (senderId !== requestExists.sender.toString()) {
    return res
      .status(400)
      .json({ msg: 'You are not authorized to access this route' });
  }

  await Request.findOneAndDelete({ _id: requestId });

  res.status(200).json({ msg: 'Request deleted' });
};

export {
  createRequest,
  getAllRequests,
  getUserSentRequest,
  getUserReceivedRequest,
  updateRequest,
  getSingleRequest,
  deleteRequest,
};
