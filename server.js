import express from 'express';
const app = express();
import dotenv from 'dotenv';
dotenv.config();
import connectDB from './db/connect.js';

//routes
import userRoutes from './routes/userRoutes.js';
import requestRoutes from './routes/requestRoutes.js';

app.use(express.json());
app.use('/api/v1/users', userRoutes);
app.use('/api/v1/requests', requestRoutes);

const port = process.env.PORT || 5000;

const start = async () => {
  try {
    const conn = await connectDB(process.env.MONGO_URL);
    console.log(`MongoDB Connected: ${conn.connection.host}`);
    app.listen(port, () => {
      console.log(`Server is listening on port ${port}...`);
    });
  } catch (error) {
    console.log(error);
  }
};

start();
